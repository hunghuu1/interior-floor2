function scrollToElement(idclick,idPoint){
    if(typeof idclick === 'string' && typeof idPoint === 'string'){
        let click = document.querySelector(idclick);
        let point = document.querySelector(idPoint);
    
        if(click && point){
            click.addEventListener('click',function(){
                point.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                    inline: "nearest",
                });
            })
        }
    }
}

scrollToElement('.banner__content','#previous');


